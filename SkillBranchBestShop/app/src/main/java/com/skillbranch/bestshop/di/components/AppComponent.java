package com.skillbranch.bestshop.di.components;

import android.content.Context;

import dagger.Component;
import com.skillbranch.bestshop.di.modules.AppModule;

@Component(modules = AppModule.class)
public interface AppComponent {
    Context getContext();
}
