package com.skillbranch.bestshop.data.rx;


import rx.Subscriber;

public class SimpleDisposableSubscriber<T> extends Subscriber<T> {

    @Override
    public void onCompleted() {

    }

    @Override
    public void onError(Throwable e) {

    }

    @Override
    public void onNext(T t) {

    }
}

