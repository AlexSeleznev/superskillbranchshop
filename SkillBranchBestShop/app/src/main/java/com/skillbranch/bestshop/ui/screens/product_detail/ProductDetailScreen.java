package com.skillbranch.bestshop.ui.screens.product_detail;

import android.content.Context;
import android.os.Bundle;
import android.view.View;

import com.skillbranch.bestshop.R;
import com.skillbranch.bestshop.data.storage.realm.ProductRealm;
import com.skillbranch.bestshop.di.DaggerService;
import com.skillbranch.bestshop.di.scopes.DaggerScope;
import com.skillbranch.bestshop.flow.AbstractScreen;
import com.skillbranch.bestshop.flow.Screen;
import com.skillbranch.bestshop.mvp.models.DetailModel;
import com.skillbranch.bestshop.mvp.presenters.AbstractPresenter;
import com.skillbranch.bestshop.mvp.presenters.IProductDetailPresenter;
import com.skillbranch.bestshop.mvp.presenters.MenuItemHolder;
import com.skillbranch.bestshop.mvp.presenters.RootPresenter;
import com.skillbranch.bestshop.ui.activities.RootActivity;
import com.skillbranch.bestshop.ui.screens.cart.CartScreen;
import com.squareup.picasso.Picasso;

import dagger.Provides;
import flow.Flow;
import flow.TreeKey;
import mortar.MortarScope;

@Screen(R.layout.screen_product_detail)
public class ProductDetailScreen extends AbstractScreen<RootActivity.RootComponent> implements TreeKey {
    public static final String TAG = "ProductDetailScreen";
    private ProductRealm mProductRealm;
    private AbstractScreen<RootActivity.RootComponent> mParentScreen;

    public ProductDetailScreen(ProductRealm product, AbstractScreen<RootActivity.RootComponent> parentScreen) {
        mProductRealm = product;
        mParentScreen = parentScreen;
    }

    @Override
    public Object createScreenComponent(RootActivity.RootComponent mParentScreen) {
        return DaggerProductDetailScreen_Component.builder()
                .rootComponent(mParentScreen)
                .module(new Module())
                .build();
    }

    @Override
    public Object getParentKey() {
        return mParentScreen;
    }

    //region ============================== DI ==============================

    @dagger.Module
    public class Module {

        @Provides
        @DaggerScope(ProductDetailScreen.class)
        ProductDetailPresenter provideProductDetailPresenter() {
            return new ProductDetailPresenter(mProductRealm);
        }

        @Provides
        @DaggerScope(ProductDetailScreen.class)
        DetailModel provideDetailModel() {
            return new DetailModel();
        }

    }

    @dagger.Component(dependencies = RootActivity.RootComponent.class, modules = Module.class)
    @DaggerScope(ProductDetailScreen.class)
    public interface Component {
        void inject(ProductDetailPresenter presenter);

        void inject(ProductDetailView view);

        DetailModel getDetailModel();
        RootPresenter getRootPresenter();
        Picasso getPicasso();
    }

    //endregion

    //region ============================== Presenter ==============================

    public class ProductDetailPresenter extends AbstractPresenter<ProductDetailView, DetailModel> implements IProductDetailPresenter {

        private final ProductRealm mProduct;

        public ProductDetailPresenter(ProductRealm productRealm) {
            mProduct = productRealm;
        }

        //region ============================== Lifecycle ==============================

        @Override
        protected void onLoad(Bundle savedInstanceState) {
            super.onLoad(savedInstanceState);
            getView().initView(mProduct);
        }

        @Override
        public void dropView(ProductDetailView view) {
//            getView().destroyViewPager();
            super.dropView(view);
        }

        @Override
        protected void initActionBar() {
            View.OnClickListener listener = item -> {
                Flow.get(getView()).set(new CartScreen());
            };

            mRootPresenter.newActionBarBuilder()
                    .setTitle(mProduct.getProductName())
                    .setBackArrow(true)
                    .addAction(new MenuItemHolder("В корзину", R.layout.icon_count_busket, listener))
                    .setTab(getView().getViewPager())
                    .build();
        }

        @Override
        protected void initDagger(MortarScope scope) {
            ((Component) scope.getService(DaggerService.SERVICE_NAME)).inject(this);
        }

        //endregion
    }


    public static class Factory {

        public static Context createChildContext(Context parentContext, AbstractScreen<ProductDetailScreen.Component> screen) {
            MortarScope parentScope = MortarScope.getScope(parentContext);
            MortarScope childScope;
            String scopeName = screen.getScopeName();

            if (parentScope.findChild(scopeName) == null) {
                childScope = parentScope.buildChild()
                        .withService(DaggerService.SERVICE_NAME,
                                screen.createScreenComponent(DaggerService.getDaggerComponent(parentContext)))
                        .build(scopeName);
            } else {
                childScope = parentScope.findChild(scopeName);
            }

            return childScope.createContext(parentContext);
        }
    }

    //endregion
}
