package com.skillbranch.bestshop.mvp.models;

import android.util.Log;

import com.skillbranch.bestshop.data.network.res.models.AddCommentRes;
import com.skillbranch.bestshop.data.storage.realm.CommentRealm;
import com.skillbranch.bestshop.jobs.SendMessageJob;

public class DetailModel extends AbstractModel {
    public void sendToServer(String id, AddCommentRes comments) {
//        mDataManager.sendCommentToServer(id, comments);
    }

    public void sendComment(String id, CommentRealm commentRealm) {
        Log.e("DetailModel", "sendComment: this");
        SendMessageJob job = new SendMessageJob(id, commentRealm);
        mJobManager.addJobInBackground(job);
    }
}
